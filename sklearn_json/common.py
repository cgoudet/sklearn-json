import json
import numpy as np


class Serializer:
    def save_attr(self, model, model_dict):
        for att in getattr(self, 'attrs_', []):
            val = getattr(model, att)

            if isinstance(val, np.ndarray):
                val = val.tolist()
            elif isinstance(val, (np.int8, np.int16, np.int32, np.int64)):
                val = int(val)
            model_dict[att] = val

    def load_attr(self, model, model_dict):
        for att in getattr(self, 'attrs_', []):
            val = model_dict[att]
            if isinstance(val, list):
                val = np.array(val)
            setattr(model, att, val)

    def to_dict(self, model):
        model_dict = {'classname':  self.__class__.__name__[:-2],
                      'params': getattr(model, 'get_params', lambda: {})()
                      }
        model_dict.update(self._to_dict(model))

        self.save_attr(model, model_dict)

        if hasattr(model, 'features'):
            model_dict['features'] = model.features

        return model_dict

    def _to_dict(self, model):
        return {}

    def from_dict(self, model_dict):
        model = self._from_dict(model_dict)
        self.load_attr(model, model_dict)

        if 'features' in model_dict:
            model.features = model_dict['features']
        return model

    def _from_dict(self, model_dict):
        return self.model_(**model_dict['params'])

    def to_json(self, model, model_name):
        with open(model_name, 'w') as model_json:
            json.dump(self.to_dict(model), model_json)

    def from_json(self, model_name):
        with open(model_name, 'r') as model_json:
            model_dict = json.load(model_json)
            return self.from_dict(model_dict)


def larray_to_list(l):
    return [x.tolist() for x in l]
