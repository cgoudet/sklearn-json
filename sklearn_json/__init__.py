import json
import pickle
from pathlib import Path
__version__ = '0.1.0'


def to_dict(model):
    serializer = factory.SerializerFactory().from_model(model)
    return serializer.to_dict(model)


def from_dict(model_dict):
    serializer = factory.SerializerFactory().from_dict(model_dict)
    return serializer.from_dict(model_dict)


def to_json(model, model_name):
    with open(model_name, 'w') as model_json:
        model_dict = to_dict(model)
        json.dump(model_dict, model_json)


def from_json(model_name):
    with open(model_name, 'r') as model_json:
        model_dict = json.load(model_json)
        return from_dict(model_dict)


def to_pickle(model, model_name):
    with open(model_name, 'wb') as f:
        model_dict = to_dict(model)
        pickle.dump(model_dict, f)


def from_pickle(model_name):
    with open(model_name, 'rb') as f:
        model_dict = pickle.load(f)
        return from_dict(model_dict)


def to_file(model, model_name):
    model_name = Path(model_name)
    writers = {'.json': to_json,
               '.pickle': to_pickle}
    writers[model_name.suffix](model, model_name)


def from_file(model_name):
    model_name = Path(model_name)
    readers = {'.json': from_json,
               '.pickle': from_pickle}
    return readers[model_name.suffix](model_name)


class ModellNotSupported(Exception):
    pass


if True:
    from . import factory
