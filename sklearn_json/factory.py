from sklearn_json import classification as clf, regression as reg


class SerializerFactory:
    def from_model(self, model):
        name = model.__class__.__name__ + 'Se'
        return self.from_name(name)()

    def from_name(self, name):
        for mod in [reg, clf]:
            try:
                return getattr(mod, name)
            except AttributeError:
                pass
        raise ValueError(f'class {name} not managed.')

    def from_dict(self, model_dict):
        name = model_dict['classname'] + 'Se'
        return self.from_name(name)()
