from .common import Serializer
from . import to_dict, from_dict
from sklearn.preprocessing import LabelBinarizer, LabelEncoder


class LabelBinarizerSe(Serializer):
    attrs_ = ['neg_label', 'pos_label', 'sparse_output', 'y_type_', 'sparse_input_', 'classes_']
    model_ = LabelBinarizer


class LabelEncoderSe(Serializer):
    attrs_ = ['classes_']
    model_ = LabelEncoder
