from .common import Serializer
from .transformers import *
import numpy as np
import scipy as sp
from sklearn import svm, discriminant_analysis, dummy
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree._tree import Tree
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, _gb_losses
from sklearn.naive_bayes import BernoulliNB, GaussianNB, MultinomialNB, ComplementNB
from sklearn.neural_network import MLPClassifier
from . import regression, csr, to_dict, from_dict
from sklearn.calibration import CalibratedClassifierCV, _CalibratedClassifier, _SigmoidCalibration
import json
import sys
from sklearn.preprocessing import LabelEncoder
import sklearn


class LogisticRegressionSe(Serializer):
    attrs_ = ['classes_', 'coef_', 'intercept_', 'n_iter_']
    model_ = LogisticRegression


class GaussianNBSe(Serializer):
    attrs_ = ['classes_', 'class_count_', 'class_prior_', 'theta_', 'sigma_', 'epsilon_']
    model_ = GaussianNB


class BernoulliNBSe(Serializer):
    attrs_ = ['classes_', 'class_count_', 'class_log_prior_', 'feature_count_', 'feature_log_prob_']
    model_ = BernoulliNB


class MultinomialNBSe(BernoulliNBSe):
    model_ = MultinomialNB


class ComplementNBSe(Serializer):
    attrs_ = ['classes_', 'class_count_', 'class_log_prior_', 'feature_count_', 'feature_log_prob_',
              'feature_all_']
    model_ = ComplementNB


class LinearDiscriminantAnalysisSe(Serializer):
    attrs_ = ['coef_', 'intercept_', 'explained_variance_ratio_', 'means_', 'priors_', 'scalings_', 'xbar_', 'classes_']
    model_ = discriminant_analysis.LinearDiscriminantAnalysis

    def _to_dict(self, model):
        serialized_model = super()._to_dict(model)
        if hasattr(model, 'covariance_'):
            serialized_model['covariance_'] = model.covariance_.tolist()

        return serialized_model

    def _from_dict(self, model_dict):
        model = super()._from_dict(model_dict)
        if 'covariance_' in model_dict:
            model.covariance_ = np.array(model_dict['covariance_'])
        return model


class QuadraticDiscriminantAnalysisSe(LinearDiscriminantAnalysisSe):
    attrs_ = ['means_', 'priors_', 'classes_']
    model_ = discriminant_analysis.QuadraticDiscriminantAnalysis

    def _to_dict(self, model):
        serialized_model = super()._to_dict(model)
        serialized_model['scalings_'] = [array.tolist() for array in model.scalings_]
        serialized_model['rotations_'] = [array.tolist() for array in model.rotations_]
        return serialized_model

    def _from_dict(self, model_dict):
        model = super()._from_dict(model_dict)
        model.rotations_ = np.array(model_dict['rotations_'])
        model.scalings_ = np.array(model_dict['scalings_'])
        return model


class SVCSe(Serializer):
    def _to_dict(self, model):
        serialized_model = {
            'meta': 'svm',
            'class_weight_': model.class_weight_.tolist(),
            'classes_': model.classes_.tolist(),
            'support_': model.support_.tolist(),
            'n_support_': model.n_support_.tolist(),
            'intercept_': model.intercept_.tolist(),
            'probA_': model.probA_.tolist(),
            'probB_': model.probB_.tolist(),
            '_intercept_': model._intercept_.tolist(),
            'shape_fit_': model.shape_fit_,
            '_gamma': model._gamma,
            'params': model.get_params()
        }

        if isinstance(model.support_vectors_, sp.sparse.csr_matrix):
            serialized_model['support_vectors_'] = csr.serialize_csr_matrix(model.support_vectors_)
        elif isinstance(model.support_vectors_, np.ndarray):
            serialized_model['support_vectors_'] = model.support_vectors_.tolist()

        if isinstance(model.dual_coef_, sp.sparse.csr_matrix):
            serialized_model['dual_coef_'] = csr.serialize_csr_matrix(model.dual_coef_)
        elif isinstance(model.dual_coef_, np.ndarray):
            serialized_model['dual_coef_'] = model.dual_coef_.tolist()

        if isinstance(model._dual_coef_, sp.sparse.csr_matrix):
            serialized_model['_dual_coef_'] = csr.serialize_csr_matrix(model._dual_coef_)
        elif isinstance(model._dual_coef_, np.ndarray):
            serialized_model['_dual_coef_'] = model._dual_coef_.tolist()

        return serialized_model

    def _from_dict(self, model_dict):
        model = svm.SVC(**model_dict['params'])
        model.shape_fit_ = model_dict['shape_fit_']
        model._gamma = model_dict['_gamma']

        model.class_weight_ = np.array(model_dict['class_weight_']).astype(np.float64)
        model.classes_ = np.array(model_dict['classes_'])
        model.support_ = np.array(model_dict['support_']).astype(np.int32)
        model.n_support_ = np.array(model_dict['n_support_']).astype(np.int32)
        model.intercept_ = np.array(model_dict['intercept_']).astype(np.float64)
        model.probA_ = np.array(model_dict['probA_']).astype(np.float64)
        model.probB_ = np.array(model_dict['probB_']).astype(np.float64)
        model._intercept_ = np.array(model_dict['_intercept_']).astype(np.float64)

        if 'meta' in model_dict['support_vectors_'] and model_dict['support_vectors_']['meta'] == 'csr':
            model.support_vectors_ = csr.deserialize_csr_matrix(model_dict['support_vectors_'])
            model._sparse = True
        else:
            model.support_vectors_ = np.array(model_dict['support_vectors_']).astype(np.float64)
            model._sparse = False

        if 'meta' in model_dict['dual_coef_'] and model_dict['dual_coef_']['meta'] == 'csr':
            model.dual_coef_ = csr.deserialize_csr_matrix(model_dict['dual_coef_'])
        else:
            model.dual_coef_ = np.array(model_dict['dual_coef_']).astype(np.float64)

        if 'meta' in model_dict['_dual_coef_'] and model_dict['_dual_coef_']['meta'] == 'csr':
            model._dual_coef_ = csr.deserialize_csr_matrix(model_dict['_dual_coef_'])
        else:
            model._dual_coef_ = np.array(model_dict['_dual_coef_']).astype(np.float64)

        return model


def serialize_dummy_classifier(model):
    model.classes_ = model.classes_.tolist()
    model.class_prior_ = model.class_prior_.tolist()
    return model.__dict__


def serialize_tree(tree):
    serialized_tree = tree.__getstate__()

    dtypes = serialized_tree['nodes'].dtype
    serialized_tree['nodes'] = serialized_tree['nodes'].tolist()
    serialized_tree['values'] = serialized_tree['values'].tolist()

    return serialized_tree, dtypes


def deserialize_tree(tree_dict, n_features, n_classes, n_outputs):
    tree_dict['nodes'] = [tuple(lst) for lst in tree_dict['nodes']]

    names = ['left_child', 'right_child', 'feature', 'threshold',
             'impurity', 'n_node_samples', 'weighted_n_node_samples']
    tree_dict['nodes'] = np.array(tree_dict['nodes'], dtype=np.dtype(
        {'names': names, 'formats': tree_dict['nodes_dtype']}))
    tree_dict['values'] = np.array(tree_dict['values'])

    tree = Tree(n_features, np.array([n_classes], dtype=np.intp), n_outputs)
    tree.__setstate__(tree_dict)

    return tree


class DecisionTreeClassifierSe(Serializer):
    attrs_ = ['max_features_', 'n_classes_', 'n_features_', 'n_outputs_', 'classes_']
    model_ = DecisionTreeClassifier
    def __init__(self):
        super().__init__()
        self.older = int(sklearn.__version__[2:4]) <= 21

    def _to_dict(self, model):
        serialized_model = super()._to_dict(model)

        if self.older:
            serialized_model['params'] = model.get_params()
            del serialized_model['params']['presort']


        tree, dtypes = serialize_tree(model.tree_)
        serialized_model['tree_'] = tree

        tree_dtypes = []
        for i in range(0, len(dtypes)):
            tree_dtypes.append(dtypes[i].str)

        serialized_model['tree_']['nodes_dtype'] = tree_dtypes

        return serialized_model

    def _from_dict(self, model_dict):
        model = super()._from_dict(model_dict)
        tree = deserialize_tree(model_dict['tree_'], model_dict['n_features_'],
                                model_dict['n_classes_'], model_dict['n_outputs_'])
        model.tree_ = tree

        return model

    #
    # class GradientBoostingClassifierSe(Serializer):
    #     def _to_dict(self, model):
    #         serialized_model = {
    #             'meta': 'gb',
    #             'classes_': model.classes_.tolist(),
    #             'max_features_': model.max_features_,
    #             'n_classes_': model.n_classes_,
    #             'n_features_': model.n_features_,
    #             'train_score_': model.train_score_.tolist(),
    #             # 'params': model.get_params(),
    #             'estimators_shape': list(model.estimators_.shape),
    #             'estimators_': []
    #         }
    #
    #         if isinstance(model.init_, dummy.DummyClassifier):
    #             serialized_model['init_'] = serialize_dummy_classifier(model.init_)
    #             serialized_model['init_']['meta'] = 'dummy'
    #         elif isinstance(model.init_, str):
    #             serialized_model['init_'] = model.init_
    #
    #         if isinstance(model.loss_, _gb_losses.BinomialDeviance):
    #             serialized_model['loss_'] = 'deviance'
    #         elif isinstance(model.loss_, _gb_losses.ExponentialLoss):
    #             serialized_model['loss_'] = 'exponential'
    #         elif isinstance(model.loss_, _gb_losses.MultinomialDeviance):
    #             serialized_model['loss_'] = 'multinomial'
    #
    #         if 'priors' in model.init_.__dict__:
    #             serialized_model['priors'] = model.init_.priors.tolist()
    #
    #         serialized_model['estimators_'] = [to_dict(reg_tree) for reg_tree in model.estimators_.reshape(-1, )]
    #
    #         return serialized_model
    #
    #     def _from_dict(self, model_dict):
    #         model = GradientBoostingClassifier(**model_dict['params'])
    #         estimators = [from_dict(tree) for tree in model_dict['estimators_']]
    #         model.estimators_ = np.array(estimators).reshape(model_dict['estimators_shape'])
    #         if 'init_' in model_dict and model_dict['init_']['meta'] == 'dummy':
    #             model.init_ = dummy.DummyClassifier()
    #             model.init_.__dict__ = model_dict['init_']
    #             model.init_.__dict__.pop('meta')
    #
    #         model.classes_ = np.array(model_dict['classes_'])
    #         model.train_score_ = np.array(model_dict['train_score_'])
    #         model.max_features_ = model_dict['max_features_']
    #         model.n_classes_ = model_dict['n_classes_']
    #         model.n_features_ = model_dict['n_features_']
    #         if model_dict['loss_'] == 'deviance':
    #             model.loss_ = _gb_losses.BinomialDeviance(model.n_classes_)
    #         elif model_dict['loss_'] == 'exponential':
    #             model.loss_ = _gb_losses.ExponentialLoss(model.n_classes_)
    #         elif model_dict['loss_'] == 'multinomial':
    #             model.loss_ = _gb_losses.MultinomialDeviance(model.n_classes_)
    #
    #         if 'priors' in model_dict:
    #             model.init_.priors = np.array(model_dict['priors'])
    #         return model
    #


class RandomForestClassifierSe(Serializer):
    attrs_ = ['max_depth', 'min_samples_split', 'min_samples_leaf', 'min_weight_fraction_leaf', 'max_features',
              'max_leaf_nodes', 'min_impurity_decrease', 'min_impurity_split', 'n_features_', 'n_outputs_', 'classes_',
              'n_classes_']
    model_ = RandomForestClassifier

    def _to_dict(self, model):
        serialized_model = super()._to_dict(model)
        serialized_model['estimators_'] = [to_dict(decision_tree) for decision_tree in model.estimators_]

        if 'oob_score_' in model.__dict__:
            serialized_model['oob_score_'] = model.oob_score_
        if 'oob_decision_function_' in model.__dict__:
            serialized_model['oob_decision_function_'] = model.oob_decision_function_.tolist()

        return serialized_model

    def _from_dict(self, model_dict):
        model = super()._from_dict(model_dict)
        estimators = [from_dict(decision_tree) for decision_tree in model_dict['estimators_']]
        model.estimators_ = np.array(estimators)

        if 'oob_score_' in model_dict:
            model.oob_score_ = model_dict['oob_score_']
        if 'oob_decision_function_' in model_dict:
            model.oob_decision_function_ = model_dict['oob_decision_function_']

        return model


class PerceptronSe(LinearDiscriminantAnalysisSe):
    attrs_ = ['coef_', 'classes_', 'n_iter_', 'intercept_']
    model_ = Perceptron


class MLPClassifierSe(Serializer):
    attrs_ = ['loss_', 'n_iter_', 'n_layers_', 'n_outputs_', 'out_activation_', 'classes_']
    model_ = MLPClassifier
    arrays_ = ['coefs_', 'intercepts_']

    def _to_dict(self, model):
        serialized_model = super()._to_dict(model)
        serialized_model['_label_binarizer'] = to_dict(model._label_binarizer)

        for c in self.arrays_:
            serialized_model[c] = [x.tolist() for x in getattr(model, c)]

        return serialized_model

    def _from_dict(self, model_dict):
        model = super()._from_dict(model_dict)
        model._label_binarizer = from_dict(model_dict['_label_binarizer'])

        for c in self.arrays_:
            setattr(model, c, np.array([np.array(x) for x in model_dict[c]]))

        return model


class CalibratedClassifierCVSe(Serializer):
    attrs_ = ['method', 'cv', 'classes_']

    def _to_dict(self, model):
        serialized_model = {
            'calibration': [to_dict(x) for x in model.calibrated_classifiers_],
            'params': {k: v for k, v in model.get_params().items() if not k.startswith('base_estimator__')},
        }
        serialized_model['params']['base_estimator'] = to_dict(model.base_estimator)
        return serialized_model

    def _from_dict(self, model_dict):
        model_dict['params']['base_estimator'] = from_dict(model_dict['params']['base_estimator'])
        model = CalibratedClassifierCV(**model_dict['params'])
        model.calibrated_classifiers_ = [from_dict(d) for d in model_dict['calibration']]

        return model


class _CalibratedClassifierSe(Serializer):
    def __init__(self):
        super().__init__()
        self.older = int(sklearn.__version__[2:4]) <= 21
        self.attrs_ = ['classes_' if self.older else 'classes']

    def _to_dict(self, model):
        out = {
            'base_estimator': to_dict(model.base_estimator),
            'calibrators_parameters': [to_dict(x) for x in model.calibrators_],
        }

        if self.older:
            out['label_encoder_'] = to_dict(model.label_encoder_)
            out['classes'] = model.classes_.tolist()
        return out

    def _from_dict(self, model_dict):
        calibrators = [from_dict(x) for x in model_dict['calibrators_parameters']]

        opts = {'base_estimator': from_dict(model_dict['base_estimator']),
                'calibrators': calibrators,
                'classes': model_dict.get('classes')}
        if self.older:
            del opts['calibrators']
            del opts['classes']

        model = _CalibratedClassifier(**opts)

        if self.older:
            model.calibrators_ = calibrators
            model.label_encoder_ = from_dict(model_dict['label_encoder_'])
        return model


class _SigmoidCalibrationSe(Serializer):
    attrs_ = ['a_', 'b_']
    model_ = _SigmoidCalibration
