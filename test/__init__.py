from sklearn.datasets import make_classification, make_regression
from sklearn.feature_extraction import FeatureHasher
import pytest
import numpy as np
import sklearn_json
from sklearn_json import *
import tempfile
from pathlib import Path
import itertools


class BaseCheckModel:
    def __init__(self, mode='clf'):
        if mode == 'clf':
            self.X, self.y = make_classification(n_samples=50, n_features=3, n_classes=3,
                                                 n_informative=3, n_redundant=0, random_state=0, shuffle=False)
        else:
            self.X, self.y = make_regression(n_samples=50, n_features=3, n_informative=3, random_state=0, shuffle=False)

        feature_hasher = FeatureHasher(n_features=3)
        features = []
        for i in range(0, 100):
            features.append({'a': np.random.randint(0, 2), 'b': np.random.randint(3, 5), 'c': np.random.randint(6, 8)})
        self.y_sparse = [np.random.randint(0, 2) for i in range(0, 100)]
        self.X_sparse = feature_hasher.transform(features)

    def _assert(self, model1, model2):
        expected_predictions = model1.predict(self.X)
        actual_predictions = model2.predict(self.X)

        np.testing.assert_array_equal(expected_predictions, actual_predictions)

    def fit(self, model, abs=False, sparse=False):
        X = self.X_sparse if sparse else self.X
        y = self.y_sparse if sparse else self.y

        if abs:
            X = np.abs(X)

        model.fit(X, y)

    def _serialize_unserialize(self, model, mode):
        with tempfile.TemporaryDirectory() as tmpdirname:
            # When
            if mode == 'dict':
                serialized_model = to_dict(model)
                return from_dict(serialized_model)
            else:
                model_name = Path(tmpdirname) / 'model.dump'
                getattr(sklearn_json, 'to_'+mode)(model, model_name)
                return getattr(sklearn_json, 'from_'+mode)(model_name)

    def check_formats(self, model, with_sparse=True, abs=False):
        modes = ['dict', 'json', 'pickle']
        sparses = list(range(1+with_sparse))
        for sparse, mode in itertools.product(sparses, modes):
            self.fit(model, abs=abs, sparse=sparse)
            deserialized_model = self._serialize_unserialize(model, mode)
            self._assert(model, deserialized_model)
