from . import *
from sklearn import svm, discriminant_analysis
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import BernoulliNB, GaussianNB, MultinomialNB, ComplementNB
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.calibration import CalibratedClassifierCV


class TestAPI:

    def setup_class(self):
        self.checker = BaseCheckModel()

    def test_bernoulli_nb(self):
        model = BernoulliNB()
        self.checker.check_formats(model)

    def test_guassian_nb(self):
        model = GaussianNB()
        # No sklearn implementation for sparse matrix
        self.checker.check_formats(model, with_sparse=False)

    def test_multinomial_nb(self):
        model = MultinomialNB()
        self.checker.check_formats(model, abs=True)

    def test_complement_nb(self):
        model = ComplementNB()
        # No sklearn implementation for sparse matrix
        self.checker.check_formats(model, abs=True, with_sparse=False)

    def test_logistic_regression(self):
        model = LogisticRegression()
        self.checker.check_formats(model)

    def test_lda(self):
        model = discriminant_analysis.LinearDiscriminantAnalysis()
        # No sklearn implementation for sparse matrix
        self.checker.check_formats(model, with_sparse=False)

    def test_qda(self):
        model = discriminant_analysis.QuadraticDiscriminantAnalysis()
        # No sklearn implementation for sparse matrix
        self.checker.check_formats(model, with_sparse=False)

    @pytest.mark.skip()
    def test_svm(self):
        self.checker.check_model(svm.SVC(gamma=0.001, C=100., kernel='linear'))
        self.checker.check_sparse_model(svm.SVC(gamma=0.001, C=100., kernel='linear'))

        model_name = 'svm.json'
        self.checker.check_model_json(svm.SVC(), model_name)
        self.checker.check_sparse_model_json(svm.SVC(), model_name)

    def test_decision_tree(self):
        model = DecisionTreeClassifier()
        self.checker.check_formats(model)

    @pytest.mark.xfail(reason='weird trouble')
    def test_gradient_boosting(self):
        self.checker.check_model(GradientBoostingClassifier(n_estimators=25, learning_rate=1.0))
        self.checker.check_sparse_model(GradientBoostingClassifier(n_estimators=25, learning_rate=1.0))

        model_name = 'gb.json'
        self.checker.check_model_json(GradientBoostingClassifier(), model_name)
        self.checker.check_sparse_model_json(GradientBoostingClassifier(), model_name)
        assert False

    def test_random_forest(self):
        model = RandomForestClassifier(n_estimators=10, max_depth=5, random_state=0)
        self.checker.check_formats(model)

    def test_perceptron(self):
        model = Perceptron()
        self.checker.check_formats(model)

    def test_mlp(self):
        model = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
        self.checker.check_formats(model)

    def test_calibrated_classifierCV(self):
        base = RandomForestClassifier(n_estimators=10, max_depth=5, random_state=0).fit(self.checker.X, self.checker.y)
        model = CalibratedClassifierCV(base_estimator=base, cv=5)
        self.checker.check_formats(model)
