from . import *
from sklearn.linear_model import LinearRegression, Lasso, Ridge
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR


class TestAPI:
    def setup_class(self):
        self.checker = BaseCheckModel('reg')

    def test_linear_regression(self):
        model = LinearRegression()
        self.checker.check_formats(model)

    def test_lasso_regression(self):
        model = Lasso(alpha=0.1)
        self.checker.check_formats(model)

    def test_ridge_regression(self):
        model = Ridge(alpha=0.5)
        self.checker.check_formats(model)

    @pytest.mark.skip()
    def test_svr(self):
        self.checker.check_model(SVR(gamma='scale', C=1.0, epsilon=0.2))
        self.checker.check_sparse_model(SVR(gamma='scale', C=1.0, epsilon=0.2))

    def test_decision_tree_regression(self):
        model = DecisionTreeRegressor()
        self.checker.check_formats(model)

    @pytest.mark.skip()
    def test_gradient_boosting_regression(self):
        self.checker.check_model(GradientBoostingRegressor())
        self.checker.check_sparse_model(GradientBoostingRegressor())

    def test_random_forest_regression(self):
        model = RandomForestRegressor(max_depth=2, random_state=0, n_estimators=100)
        self.checker.check_formats(model)

    def test_mlp_regression(self):
        model = MLPRegressor()
        self.checker.check_formats(model)
