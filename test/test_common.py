from . import *
from sklearn.linear_model import LogisticRegression


def test_from_file():
    checker = BaseCheckModel()
    model = LogisticRegression()
    checker.fit(model)

    with tempfile.TemporaryDirectory() as tmpdirname:
        for format in ['.json', '.pickle']:
            outname = Path(tmpdirname) / ('model'+format)

            to_file(model, outname)
            deserialized_model = from_file(outname)
            checker._assert(model, deserialized_model)

            if format == '.json':
                deserialized_model = from_json(outname)
            elif format == '.pickle':
                deserialized_model = from_pickle(outname)
            else:
                assert False
            checker._assert(model, deserialized_model)
